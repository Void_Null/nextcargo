package com.ub.core.persistance;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;
import java.time.ZoneId;

@Data
public abstract class PersistanceEntity<ID> {
    @Id
    private ID id;
    private LocalDateTime createdAt = LocalDateTime.now();
    private LocalDateTime updatedAt = LocalDateTime.now();

    private ZoneId timeZone = ZoneId.systemDefault();
}

