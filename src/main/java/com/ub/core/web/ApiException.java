package com.ub.core.web;

abstract class ApiException extends Exception {
    private ApiError error;

    ApiException(String message) {
        super(message);
        this.error = new ApiError(message);
    }

    public ApiError getError() {
        return error;
    }
}
