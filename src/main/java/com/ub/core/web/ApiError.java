package com.ub.core.web;

import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class ApiError {
    private LocalDateTime timestamp;
    private String message;
    private String debugMessage;

    public ApiError() {
        this.timestamp = LocalDateTime.now();
    }

    ApiError(String message) {
        this.timestamp = LocalDateTime.now();
        this.message = message;
    }
}
