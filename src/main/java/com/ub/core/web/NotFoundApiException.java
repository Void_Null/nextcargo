package com.ub.core.web;

public class NotFoundApiException extends ApiException {

    public NotFoundApiException(String message) {
        super(message);
    }
}
