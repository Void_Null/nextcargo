package com.ub.nextcargo.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Customer {
    String id;
    String lastName;
    String firstName;
    String middleName;
    String birthDay;

    public Customer(String lastName, String firstName, String middleName, String birthDay){
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.birthDay = birthDay;
    }
}
