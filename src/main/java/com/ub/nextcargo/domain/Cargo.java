package com.ub.nextcargo.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.stream.Location;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class Cargo {
    String id;
    Customer customer;
    Location origin;
    Location destination;
    LocalDate originDate;
    LocalDate destinationDate;
    Double weight;
    Itinerary itinerary;
    Delivery delivery;

    public Cargo(Customer costumer, Location origin, Location destination,
                 LocalDate originDate, LocalDate destinationDate, Double weight,
                 Itinerary itinerary, Delivery delivery){

    }

}
