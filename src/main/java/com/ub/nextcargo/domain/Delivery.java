package com.ub.nextcargo.domain;

import lombok.Data;
import lombok.Getter;

import javax.xml.stream.Location;
import java.time.LocalDateTime;

@Data
public final class Delivery {
    Location lastKnownLocation;
    enum transportationStatus{
        NOT_RECEIVED,
        IN_PORT,
        ON_BOARD,
        CLAIMED,
        UNKNOWN
    }
    enum  routingStatus{
        NOT_ROUTED,
        ROUTED,
        MISROUTED
    }
    LocalDateTime calculatedAt;


}
