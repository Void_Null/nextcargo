package com.ub.nextcargo.domain;

import lombok.Getter;

import java.util.List;

@Getter
public final class Itinerary {
    final List<RouteStage> routeStages;

    public Itinerary(List<RouteStage> routeStages){
        this.routeStages = routeStages;
    }

}
