package com.ub.nextcargo.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Voyage {
    String id;
    String number;
    Shedule shedule;

    public Voyage(String number, Shedule shedule){
        this.number = number;
        this.shedule = shedule;
    }
}
