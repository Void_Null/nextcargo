package com.ub.nextcargo.domain;

import javax.xml.stream.Location;
import java.time.LocalDateTime;

public final class RouteStage {
    final Voyage currentVoyage;
    final Location laslKnownLocation;
    final Location unloadLocation;
    final LocalDateTime loadTime;
    final LocalDateTime unloadTime;

    public RouteStage(Voyage voyage, Location laslKnownLocation, Location unloadLocation,
                    LocalDateTime loadTime, LocalDateTime unloadTime){
        this.currentVoyage = voyage;
        this.laslKnownLocation = laslKnownLocation;
        this.loadTime = loadTime;
        this.unloadLocation = unloadLocation;
        this.unloadTime = unloadTime;
    }
}
