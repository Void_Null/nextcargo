package com.ub.nextcargo.domain;

import lombok.Getter;

import javax.xml.stream.Location;
import java.time.LocalDateTime;

@Getter
public final class CarrierMovement {
    final Location departureLocation;
    final Location arrivalLocation;
    final LocalDateTime departureTime;
    final LocalDateTime arrivalTime;

    public CarrierMovement(Location departureLocation, Location arrivalLocation,
                           LocalDateTime departureTime, LocalDateTime arrivalTime){
        this.departureLocation = departureLocation;
        this.arrivalLocation = arrivalLocation;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
    }

}
