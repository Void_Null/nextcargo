package com.ub.nextcargo;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@RequiredArgsConstructor
@SpringBootApplication(scanBasePackages = "com.ub")
public class NextcargoApplication {

    public static void main(String[] args) {
        SpringApplication.run(NextcargoApplication.class, args);
    }

}
