package com.ub.nextcargo.persistance.location;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LocationRepository extends MongoRepository<LocationDoc, ObjectId> {
}
