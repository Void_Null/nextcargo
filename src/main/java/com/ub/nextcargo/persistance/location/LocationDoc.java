package com.ub.nextcargo.persistance.location;

import com.ub.core.persistance.PersistanceEntity;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class LocationDoc extends PersistanceEntity<ObjectId> {
    private String name;
}
