package com.ub.nextcargo.persistance.cargo;

import com.ub.core.persistance.PersistanceEntity;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Document
public class CargoDoc extends PersistanceEntity<ObjectId> {
    private ObjectId customerId;
    private ObjectId originId;
    private ObjectId destinationId;
    private LocalDateTime originDate;
    private LocalDateTime destinationDate;
    private Double weight;
    private ObjectId deliveryId;

}
