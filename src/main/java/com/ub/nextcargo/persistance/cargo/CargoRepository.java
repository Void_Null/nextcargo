package com.ub.nextcargo.persistance.cargo;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CargoRepository extends MongoRepository<CargoDoc, ObjectId> {

}
