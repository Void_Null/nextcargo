package com.ub.nextcargo.persistance.delivery;

import com.ub.core.persistance.PersistanceEntity;
import lombok.Data;
import org.bson.types.ObjectId;

import javax.xml.stream.Location;
import java.time.LocalDateTime;

@Data
public class DeliveryDoc extends PersistanceEntity<ObjectId> {
    public final class Delivery {
        Location lastKnownLocation;
        LocalDateTime calculatedAt;
    }
}
