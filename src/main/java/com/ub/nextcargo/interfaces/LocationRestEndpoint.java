package com.ub.nextcargo.interfaces;

import com.ub.core.web.NotFoundApiException;
import com.ub.nextcargo.application.LocationStoringFacade;
import com.ub.nextcargo.application.dto.location.LocationDTO;
import com.ub.nextcargo.application.dto.location.LocationRequest;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class LocationRestEndpoint {
    private final LocationStoringFacade locationStoring;

    @GetMapping(value = Routes.LOC, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<LocationDTO>> getAllLocations(@RequestParam int limit,
                                                           @RequestParam int offset) {
        return ResponseEntity.ok(locationStoring.getAllLocations(limit, offset));
    }

    @PostMapping(value = Routes.LOC, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createCargo(@RequestBody LocationRequest request) {
        return ResponseEntity.ok(locationStoring.createNewLocation(request).toString());
    }


    @GetMapping(value = Routes.LOC_BY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LocationDTO> getCargo(@PathVariable ObjectId id)
            throws NotFoundApiException {
        return ResponseEntity.ok(locationStoring.getLocation(id));
    }

    @PutMapping(value = Routes.LOC_BY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectId> editCargo(@PathVariable ObjectId id,
                                              @RequestBody LocationRequest request) {
        return ResponseEntity.ok(locationStoring.editLocation(id, request));
    }

    @DeleteMapping(value = Routes.LOC_BY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean deleteCargo(@PathVariable ObjectId id){
        return locationStoring.deleteLocation(id);
    }

}
