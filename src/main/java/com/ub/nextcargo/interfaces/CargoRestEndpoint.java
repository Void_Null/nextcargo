package com.ub.nextcargo.interfaces;

import com.ub.core.web.NotFoundApiException;
import com.ub.nextcargo.application.CargoBookingFacade;
import com.ub.nextcargo.application.dto.cargo.CargoDTO;
import com.ub.nextcargo.application.dto.cargo.CargoRequest;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class CargoRestEndpoint {
    private final CargoBookingFacade cargoBooking;

    @GetMapping(value = Routes.ROOT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CargoDTO>> getAllCargoes(@RequestParam int limit,
                                                        @RequestParam int offset) {
        return ResponseEntity.ok(cargoBooking.getAllCargoes(limit, offset));
    }

    @PostMapping(value = Routes.ROOT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createCargo(@RequestBody CargoRequest request) {
        return ResponseEntity.ok(cargoBooking.createNewCargo(request).toString());
    }

    //новое
    @GetMapping(value = Routes.BY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CargoDTO> getCargo(@PathVariable ObjectId id)
            throws NotFoundApiException {
        return ResponseEntity.ok(cargoBooking.getCargo(id));
    }

    @PutMapping(value = Routes.BY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectId> editCargo(@PathVariable ObjectId id,
                                              @RequestBody CargoRequest request) {
        return ResponseEntity.ok(cargoBooking.editCargo(id, request));
    }

    @DeleteMapping(value = Routes.BY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean deleteCargo(@PathVariable ObjectId id){
        return cargoBooking.deleteCargo(id);
    }
}
