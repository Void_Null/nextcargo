package com.ub.nextcargo.interfaces;

class Routes {
    private static final String API = "/api/v1";

    static final String ROOT = API + "/cargoes";

    static final String BY_ID = ROOT + "/{id}";

    static final String LOC = API + "/locations";

    static  final String LOC_BY_ID = LOC + "/{id}";
}
