package com.ub.nextcargo.application;

import com.ub.core.web.NotFoundApiException;
import com.ub.nextcargo.application.dto.location.LocationDTO;
import com.ub.nextcargo.application.dto.location.LocationRequest;
import org.bson.types.ObjectId;

import java.util.List;

public interface LocationStoringFacade {
    ObjectId createNewLocation(LocationRequest request);

    List<LocationDTO> getAllLocations(int limit, int offset);

    ObjectId editLocation(ObjectId locationId, LocationRequest request);

    LocationDTO getLocation(ObjectId locationId) throws NotFoundApiException;

    boolean deleteLocation(ObjectId locationId);
}
