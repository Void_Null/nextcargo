package com.ub.nextcargo.application;

import com.ub.core.web.NotFoundApiException;
import com.ub.nextcargo.application.dto.cargo.CargoDTO;
import com.ub.nextcargo.application.dto.cargo.CargoRequest;
import org.bson.types.ObjectId;

import java.util.List;

public interface  CargoBookingFacade {
    ObjectId createNewCargo(CargoRequest request);

    List<CargoDTO> getAllCargoes(int limit, int offset);

    ObjectId editCargo(ObjectId cargoId, CargoRequest request);

    CargoDTO getCargo(ObjectId cargoId) throws NotFoundApiException;

    boolean deleteCargo(ObjectId cargoId);

}
