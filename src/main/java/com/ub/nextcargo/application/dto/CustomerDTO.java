package com.ub.nextcargo.application.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class CustomerDTO {
    String lastName;
    String firstName;
    String middleName;
    LocalDate birthDay;

    public CustomerDTO(String lastName, String firstName, String middleName, LocalDate birthDay) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.birthDay = birthDay;
    }
}
