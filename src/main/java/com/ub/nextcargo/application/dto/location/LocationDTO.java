package com.ub.nextcargo.application.dto.location;

import lombok.Data;
import lombok.Getter;

//@Getter
@Data

public class LocationDTO {
    private String name;

    public LocationDTO() {
    }

    public LocationDTO(String name) {
        this.name = name;
    }
}
