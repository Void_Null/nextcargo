package com.ub.nextcargo.application.dto.cargo;

import com.ub.nextcargo.application.dto.CustomerDTO;
import com.ub.nextcargo.application.dto.location.LocationDTO;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class  CargoDTO {
    private CustomerDTO customer;
    private LocationDTO origin;
    private LocationDTO destination;
    private LocalDateTime originDate;
    private LocalDateTime destinationDate;
    private Double weight;
}
