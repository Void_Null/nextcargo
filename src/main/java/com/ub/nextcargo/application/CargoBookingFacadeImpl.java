package com.ub.nextcargo.application;

import com.ub.core.web.NotFoundApiException;
import com.ub.nextcargo.application.dto.cargo.CargoDTO;
import com.ub.nextcargo.application.dto.cargo.CargoRequest;
import com.ub.nextcargo.application.dto.CustomerDTO;
import com.ub.nextcargo.application.dto.location.LocationDTO;
import com.ub.nextcargo.persistance.cargo.CargoDoc;
import com.ub.nextcargo.persistance.cargo.CargoRepository;
import org.bson.types.ObjectId;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CargoBookingFacadeImpl implements CargoBookingFacade {
    private final CargoRepository cargoRepository;

    public CargoBookingFacadeImpl(CargoRepository cargoRepository) {
        this.cargoRepository = cargoRepository;
    }

    @Override
    public ObjectId createNewCargo(CargoRequest request) {
        CargoDoc doc = new CargoDoc();
        doc.setCustomerId(request.getCustomerId());
        doc.setOriginId(request.getOriginId());
        doc.setDestinationId(request.getDestinationId());
        doc.setOriginDate(request.getOriginDate());
        doc.setDestinationDate(request.getDestinationDate());
        doc.setWeight(request.getWeight());
        cargoRepository.save(doc);
        return doc.getId();
    }

    @Override
    public List<CargoDTO> getAllCargoes(int limit, int offset) {
        return cargoRepository.findAll(PageRequest.of(offset, limit))
                .stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    private CargoDTO map(CargoDoc cargoDoc) {
        CargoDTO cargo = new CargoDTO();
        cargo.setCustomer(new CustomerDTO("Ivanov", "I", "I", LocalDate.now().minusYears(20)));
        cargo.setOrigin(new LocationDTO("town"));
        cargo.setDestination(new LocationDTO("destination town"));
        cargo.setOriginDate(cargoDoc.getOriginDate());
        cargo.setDestinationDate(cargoDoc.getDestinationDate());
        cargo.setWeight(cargoDoc.getWeight());
        return cargo;
    }

    @Override
    public ObjectId editCargo(ObjectId cargoId, CargoRequest request) {
        CargoDoc cargo = cargoRepository.findById(cargoId)
                .orElseThrow(IllegalArgumentException::new);

        cargo.setCustomerId(request.getCustomerId());
        cargo.setOriginId(request.getOriginId());
        cargo.setDestinationId(request.getDestinationId());
        cargo.setOriginDate(request.getOriginDate());
        cargo.setDestinationDate(request.getDestinationDate());
        cargo.setWeight(request.getWeight());
        cargoRepository.save(cargo);
        return cargo.getId();

    }

    @Override
    public CargoDTO getCargo(ObjectId cargoId) throws NotFoundApiException {
        return cargoRepository.findById(cargoId)
                .map(this::map)
                .orElseThrow(() -> new NotFoundApiException("Cargo with id " + cargoId + " not found"));
    }

    @Override
    public boolean deleteCargo(ObjectId cargoId) {

        if (cargoRepository.existsById(cargoId)){
            cargoRepository.deleteById(cargoId);
            return true;
        }
        else return false;

    }
}
