package com.ub.nextcargo.application;

import com.ub.core.web.NotFoundApiException;
import com.ub.nextcargo.application.dto.location.LocationDTO;
import com.ub.nextcargo.application.dto.location.LocationRequest;
import com.ub.nextcargo.persistance.location.LocationDoc;
import com.ub.nextcargo.persistance.location.LocationRepository;
import org.bson.types.ObjectId;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class LocationStoringFacadeImpl implements LocationStoringFacade{
    private final LocationRepository locationRepository;

    public LocationStoringFacadeImpl(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }


    @Override
    public ObjectId createNewLocation(LocationRequest request) {
        LocationDoc doc = new LocationDoc();
        doc.setName(request.getName());
        locationRepository.save(doc);
        return doc.getId();
    }

    @Override
    public List<LocationDTO> getAllLocations(int limit, int offset) {
        return locationRepository.findAll(PageRequest.of(offset, limit))
                .stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    private LocationDTO map(LocationDoc locationDoc){
        LocationDTO locationDTO = new LocationDTO();
        locationDTO.setName(locationDoc.getName());
        return locationDTO;
    }

    @Override
    public ObjectId editLocation(ObjectId locationId, LocationRequest request) {
        LocationDoc doc = locationRepository.findById(locationId)
                .orElseThrow(IllegalArgumentException::new);
        doc.setName(request.getName());
        locationRepository.save(doc);
        return doc.getId();
    }

    @Override
    public LocationDTO getLocation(ObjectId locationId) throws NotFoundApiException {
        return locationRepository.findById(locationId)
                .map(this::map)
                .orElseThrow(() -> new NotFoundApiException("Location with id " + locationId + " not found"));

    }

    @Override
    public boolean deleteLocation(ObjectId locationId) {

        if (locationRepository.existsById(locationId)){
            locationRepository.deleteById(locationId);
            return true;
        }
        else return false;
    }
}
